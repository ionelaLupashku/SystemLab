package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jdk.internal.util.xml.impl.Input;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main extends Application {
    private Stage primaryStage;
    private BorderPane rootLayol;

    public Main(){

    }

    public static void main(String[] args) {
        launch(args);
    }
    TextArea text;
    @Override
    public void start(Stage primaryStage) throws Exception {

        try {
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root);

            text = new TextArea();
            root.setRight(text);

            Button btnRead = new Button("Відкрити");
            Button btnClear = new Button("Очистити");
            Button btnRemake = new Button("Переробити");
            Button btnWrite = new Button("Зберегти");
            GridPane noRoot = new GridPane();
            noRoot.add(btnWrite, 0, 0);
            noRoot.add(btnClear, 0, 1);
            noRoot.add(btnRemake, 0, 2);
            noRoot.add(btnRead, 0, 3);
            root.setCenter(noRoot);

            btnRead.setOnAction(new ReadButton());
            btnClear.setOnAction(new ClearButton());
            btnRemake.setOnAction(this::eventSomething);
            btnWrite.setOnAction(new WriteButton());

            primaryStage.setScene(scene);
            this.primaryStage=primaryStage;
            this.rootLayol=root;
            System.out.println(this);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @FXML
    public void eventSomething(ActionEvent e){
        try {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("Rename.fxml"));
        AnchorPane page =(AnchorPane) loader.load();

        // New window (Stage)
        Stage newWindow = new Stage();
        newWindow.setTitle("Rename");
        newWindow.initModality(Modality.WINDOW_MODAL);
        newWindow.initOwner(primaryStage);

        Scene secondScene = new Scene(page);
        newWindow.setScene(secondScene);
        ControlerRename controlerRename= new FXMLLoader().getController();
        System.out.println(this.text.getText());
        System.out.println(controlerRename==null);
        controlerRename.setMain(this);
        // Set position of second window, related to primary window.
        newWindow.setX(300);
        newWindow.setY(300);

        TextField mask=(TextField)page.lookup("#mask");
        newWindow.show();
        }catch(Exception e1)
            {e1.printStackTrace();}
    }


    public List<String> read(){
        List<String> list = new ArrayList<String>();
        FileChooser choose = new FileChooser();
        File txt = choose.showOpenDialog(null);
        try (Scanner in = new Scanner (txt)){
            while(in.hasNext()){
                list.add(in.nextLine());

            }

        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return list;
    }

    public String getText(){
        return text.getText();
    }

    public void setText(String str){
        text.setText(str);
    }
    private class ReadButton implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent arg0) {
            text.setText(read().toString());

        }
    }
    private class ClearButton implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent arg0) {
            text.clear();

        }
    }


    private class WriteButton implements EventHandler<ActionEvent>{


        @Override
        public void handle(ActionEvent event) {
            File file = new File("test");
            try (FileOutputStream filtrom = new FileOutputStream(file);
                 ObjectOutputStream objektstrom = new ObjectOutputStream(filtrom)) {
                String res = text.getText();
                objektstrom.writeObject(res);
            } catch (FileNotFoundException e) {
                System.err.println("не удалось создать файл " + file.getName());
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println("Проблема с записью в файл " + file.getName());
                e.printStackTrace();
            }
        }

    }
}
