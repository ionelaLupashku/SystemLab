package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;


public class ControlerRename implements Initializable{

    @FXML private javafx.scene.control.Button closeButton;
    @FXML private javafx.scene.control.Button rename1;
    @FXML private javafx.scene.control.Button rename2;
    @FXML private javafx.scene.control.TextField renameText;
    @FXML private javafx.scene.control.TextField mask;
    @FXML private Main main;

    public  ControlerRename(){
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    Boolean isMetha(String c){
        return ((c.charAt(0)=='{')&&(Character.isDigit(c.charAt(1)))&&(c.charAt(1)!='0')&&(c.charAt(2)==',')
                &&(Character.isDigit(c.charAt(3)))&&(c.charAt(3)!='0')&&(c.charAt(4)=='}')&&(c.charAt(1)<=c.charAt(3)));
    }

    Boolean func(String temp,String word){
        Boolean f=Boolean.TRUE,f1=Boolean.FALSE;
        int i=0,j=0;
        for(;i<temp.length() && j<word.length() && f;++i){
            if(!isMetha(temp.substring(i,5))){
                if(temp.charAt(i)!=word.charAt(j))
                    return Boolean.FALSE;
                else j++;
            }else{
                int Tmin=temp.charAt(i+1)-'0';
                int Tmax=temp.charAt(i+3)-'0';
                for(int i1=Tmin;i1<=Tmax && (i1+j)<=word.length();++i1){
                    if(!f1)
                        f1=func(temp.substring(i+5,temp.length()-i-5),word.substring(j+i1,word.length()-j-i1));
                }
                return f1;
            }
        }
        if(j!=word.length() || i!=temp.length())
            return Boolean.FALSE;
        return f;

    }
    String changeWord(String temp,String word){
        int i=0,j=0;
        int t=0;
        StringBuffer rezult=new StringBuffer();
        for(;i<temp.length();++i){
            if(!isMetha(temp.substring(i,5))){
                rezult.insert(j,temp.substring(i,1));
                ++t;
                j++;
            }
            else{
                int Tmax=temp.charAt(i+3)-48;
                while(word.charAt(t)==word.charAt(t+1)) ++t;
                for(int u=0;u<Tmax;++u){
                    if(t<word.length()) rezult.insert(j,word.substring(t,1));
                    else {
                        --t;
                        rezult.insert(j,word.substring(t,1));
                    }
                    ++j;
                }
                ++t;
                i+=4;
            }
        }

        return rezult.toString();
    }
    @FXML
    private void closeButtonAction(){
        // get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
    int it=0;
    @FXML
    private void renameButtonAction(){
       String mymask=mask.getText();
       String text=main.getText();

        String temp="";
        String word="";
        Boolean f=Boolean.TRUE;
        while (f) {
            while (text.charAt(it) != ' ' && it < text.length()) {
                temp += text.charAt(it);
                it++;
            }
            if(!func(mymask,temp)){
                f=Boolean.TRUE;
            } else {
                System.out.println(temp);
                word=changeWord(mymask,temp);
                renameText.setText(word);
            }
        }

    }

    public void setMain(Main main){
        this.main=main;
    }


}

